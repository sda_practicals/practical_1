//
//  main.cpp
//  practical_1_1
//
//  Created by Jacob Adams on 30/11/2023.
//

#include <iostream>

// Function to calculate factorial
int calculateFactorial(int n) {
    if (n == 0 || n == 1) {
        return 1;
    } 
    else {
        return n * calculateFactorial(n - 1);
    }
}

int main() {
    // Get input from the user
    std::cout << "Enter an integer: ";

    int userNumber;

    // Read the user input
    if (!(std::cin >> userNumber)) {
        std::cerr << "Invalid input. Please enter a valid integer." << std::endl;
        return 1;
    }

    // Calculate factorial
    int result = calculateFactorial(userNumber);

    // Display the result
    std::cout << "Factorial of " << userNumber << " is: " << result << std::endl;

    return 0;
}
