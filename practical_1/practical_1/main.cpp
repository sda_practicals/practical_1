//
//  main.cpp
//  practical_1
//
//  Created by Jacob Adams on 30/11/2023.
//

#include <iostream>

int main() {
    /** Get input from the user
    */
    std::cout << "Enter an integer: ";
    
    int userNumber;
    
    /** Read the user input
     * alerts use if input is invalid
     */
    if (!(std::cin >> userNumber)) {
        std::cerr << "Invalid input. Please enter a valid integer." << std::endl;
        return 1;
    }

    /** Display values up to the entered number
     * declares variable i as 1
     * increments value up to i (user input)
    */
    for (int i = 1; i <= userNumber; ++i) {
        std::cout << i << std::endl;
    }

    return 0;
}

